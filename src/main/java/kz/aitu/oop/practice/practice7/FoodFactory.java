package kz.aitu.oop.practice.practice7;

public class FoodFactory {
    public Food getFood(String order) {
        if (order.equalsIgnoreCase("cake")) {
            Food c = new Cake();
            return c;
        }
        else {
            Food p = new Pizza();
            return p;
        }

    }
}
