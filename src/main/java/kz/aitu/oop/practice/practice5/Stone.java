package kz.aitu.oop.practice.practice5;

public abstract class Stone {
    private String name;
    private double weight;
    private double price;

    //constructor
    public Stone() {
        this.name = name;
        this.weight = weight;
        this.price = price;
    }

    //getter and setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    // toString

    @Override
    public String toString() {
        return   "name='" + name + '\'' +
                ", weight=" + weight +
                ", price=" + price +
                '}';
    }
}
