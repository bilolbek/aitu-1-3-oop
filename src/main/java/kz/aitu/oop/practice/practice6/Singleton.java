package kz.aitu.oop.practice.practice6;

public class Singleton {
    private static final Singleton INSTANCE = new Singleton();

    String str = "Hello I am a singleton! Let me say hello world to you";

    private Singleton() {
    }

    public static Singleton getSingleInstance() {
        return INSTANCE;
    }
}
