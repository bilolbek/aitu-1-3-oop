package kz.aitu.oop.practice.practice4;

public class Fish extends Aquarium {
    String nameOfFish;

    public Fish(double price, double weight, String nameOfFish) {
        super(price, weight);
    }

    public Fish(double price, double weight) {
        super(price, weight);
    }

    public Fish() {

    }

    public Fish(double price) {
        super(price);
    }

    public Fish(double price, String nameOfFish) {
        super(price);
    }
}
