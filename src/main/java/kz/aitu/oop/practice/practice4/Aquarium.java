package kz.aitu.oop.practice.practice4;

public class Aquarium {
     double price = 0;
     double weight = 0;

    private static double totalPrice = 0;

    public Aquarium(double price, double weight) {
        this.price = price;
        this.weight = weight;
        totalPrice += this.price;
    }

    public Aquarium() {
    }

    public Aquarium(double price) {
        this.price = price;
        totalPrice += this.price;
    }

    public static double getTotalPrice() {
        return totalPrice;
    }

    public static void main(String[] args) {

        Aquarium obj1 = new Aquarium(2, 20);
        Aquarium obj2 = new Aquarium(3, 20);
        Aquarium obj3 = new Aquarium(20, 20);
        Aquarium obj4 = new Aquarium();
        Aquarium obj20 = new Aquarium(20);

        Fish fish1 = new Fish(2, 20, "Fish 1");
        Fish fish2 = new Fish(2, 20);
        Fish fish3 = new Fish();
        Fish fish4 = new Fish(20);

        Reptile reptile1 = new Reptile(20);

        Accessory accessory1 = new Accessory(63.20, 2, "Лампа");

        System.out.println("Total price " + getTotalPrice());
    }

}
