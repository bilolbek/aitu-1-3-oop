package kz.aitu.oop.practice.practice4;

public class Accessory extends Aquarium {
    String nameOfAccessory;

    public Accessory(double price, double weight, String nameOfAccessory) {
        super(price, weight);
    }

    public Accessory(double price, double weight) {
        super(price, weight);
    }

    public Accessory() {

    }

    public Accessory(double price) {
        super(price);
    }

    public Accessory(double price, String nameOfAccessory) {
        super(price);
    }
}