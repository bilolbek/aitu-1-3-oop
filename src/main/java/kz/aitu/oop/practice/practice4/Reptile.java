package kz.aitu.oop.practice.practice4;

public class Reptile extends Aquarium {
    String nameOfReptile;

    public Reptile(double price, double weight, String nameOfReptile) {
        super(price, weight);
    }

    public Reptile(double price, double weight) {
        super(price, weight);
    }

    public Reptile() {

    }

    public Reptile(double price) {
        super(price);

    }
}
