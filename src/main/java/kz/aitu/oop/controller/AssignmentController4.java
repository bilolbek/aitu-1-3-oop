package kz.aitu.oop.controller;

import kz.aitu.oop.entity.Student;
import kz.aitu.oop.repository.StudentFileRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

//import kz.aitu.oop.repository.StudentDBRepository;

@RestController
@RequestMapping("/api/task/4")
@AllArgsConstructor
public class AssignmentController4 {


    /**
     * @param group
     * @return all student name by group name
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}")
    public ResponseEntity<?> getStudentsByGroup(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student : studentFileRepository.getStudents()) {
            if (student.getGroup().equals(group)) {
                result += student.getName() + "<br>";
            }
        }

        return ResponseEntity.ok(result);
    }

    /**
     * @param group
     * @return stats by point letter (counting points): example  A-3, B-4, C-1, D-1, F-0
     * @throws FileNotFoundException
     */
    @GetMapping("/group/{group}/stats")
    public ResponseEntity<?> getGroupStats(@PathVariable("group") String group) throws FileNotFoundException {

        //write your code here
        String result = "";
        int A = 0, B = 0, C = 0, D = 0, F = 0;
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for (Student student : studentFileRepository.getStudents()) {
            if (student.getGroup().equals(group)) {
                if ((student.getPoint() > 89) && (student.getPoint() <= 100)) {
                    A++;
                }
                if ((student.getPoint() < 90) && (student.getPoint() > 79)) {
                    B++;
                }
                if (student.getPoint() < 80 && student.getPoint() > 69) {
                    C++;
                }
                if (student.getPoint() < 70 && student.getPoint() > 59) {
                    D++;
                }
                if (student.getPoint() < 60) {
                    F++;
                }
            }
        }
        result += "<b>Statistcs : A</b>-" + A + ", " + "<b>B</b>-" + B + ", " + "<b>C</b>-" + C + ", " + "<b>D</b>-" + D + ", " + "<b>F</b>-" + F;
        return ResponseEntity.ok(result);
    }

    /**
     * @return top 5 students name by point
     * @throws FileNotFoundException
     */
    @GetMapping("/students/top")
    public ResponseEntity<?> getTopStudents() throws FileNotFoundException {
        String result = "";
        //write your code here
        String[] top5 = new String[5];
        double max;
        double tmax = 100;
        String ttop = "";
        StudentFileRepository studentFileRepository = new StudentFileRepository();
        for(int i=0; i<5;i++) {
            max = 0;
            for (Student student : studentFileRepository.getStudents()) {
                if (max < student.getPoint() && tmax > student.getPoint()){
                    max = student.getPoint();
                    ttop = student.getName();
                }
            }
            tmax = max;
            top5[i]=ttop+" "+max;
        }
        for (int i = 0; i < 5; i++){
            result+=top5[i] + "<br>";
        }


        return ResponseEntity.ok(result);
    }
}
