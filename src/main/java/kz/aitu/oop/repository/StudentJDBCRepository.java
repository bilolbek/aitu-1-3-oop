package kz.aitu.oop.repository;

import kz.aitu.oop.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentJDBCRepository {

    public List<Student> getStudents() {

        List<Student> studentList = new ArrayList<>();

        String url = "jdbc:mysql://localhost:3306/aitu";
        String username = "root";
        String password = "123456";

        try {
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from student");

            while(resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setName(resultSet.getString("name"));
                student.setAge(resultSet.getInt("age"));
                student.setPoint(resultSet.getDouble("point"));

                studentList.add(student);
            }

            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return studentList;
    }
}
