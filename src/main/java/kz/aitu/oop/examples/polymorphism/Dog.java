package kz.aitu.oop.examples.polymorphism;

public class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    public void run() {
        System.out.println("Dog " + getName() + " running");
    }

    public void run(int speed) {
        System.out.println("Dog running " + speed + "m/s");
    }
}
