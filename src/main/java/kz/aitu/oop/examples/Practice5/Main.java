package kz.aitu.oop.examples.Practice5;

import java.io.File;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void example() throws ExceptionBAH {
        System.out.println("Throwing MyException from example");
        throw new ExceptionBAH("From example");
    }
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        /////////////////////////// task 1
        /*
        try {
            example();
        } catch (ExceptionBAH e) {
            System.err.println("Caught Exeption BAH");
            e.printStackTrace();
        } finally {
            System.out.println("Exeptionnan ottikko glavnoe");
        }
*/
        //////////////////////// task 2

        System.out.print("Enter an integer: ");
        int i = isInt();
        System.out.println("You entered " + i);
    }
    public static int isInt() { // точно так же можно для isDouble + добавив одно  условие
        while (true) {
            try {
                return sc.nextInt();
            } catch (InputMismatchException e) {
                sc.next();
                System.out.print("That’s not "+ "an integer. Try again: ");
            }
        }
    }
}
