package kz.aitu.oop.examples.Assigment7;

public abstract class Shape {
    private String color;
    private boolean filled;

    public Shape() {
        color = "green";
        filled = true;
    }

    public Shape(String cin, boolean fin) {
        color = cin;
        filled = fin;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String cSet) {
        color = cSet;
    }

    public boolean isFilled() {
        if (filled == true) {
            return true;
        } else {
            return false;
        }
    }

    public void setFilled(boolean fSet) {
        filled = fSet;
    }

    public String toString() {
        String not = "";
        if (isFilled() == false) {
            not = " not";
        }
        return "Shape => color: " + color + ", " + not + " filled. ";
    }

    public abstract double getArea();

    public abstract double getPerimeter();

}

