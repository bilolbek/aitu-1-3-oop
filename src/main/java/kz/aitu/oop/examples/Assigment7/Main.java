package kz.aitu.oop.examples.Assigment7;

public class Main {
    public static void main(String[] args) {
        Shape s1 = new Circle(5.5, "red", false);  // Upcast Circle to Shape
        System.out.println(s1);                    // which version?
        System.out.println(s1.getArea());          // which version?
        System.out.println(s1.getPerimeter());     // which version?
        System.out.println(s1.getColor());
        System.out.println(s1.isFilled());

        /*
                System.out.println(s1.getRadius());
                Returns an error, because the Shape class does not have getter "getRadius" even if you
                create an abstract class "getRadius", you should have written this in all classes where
                extends Shape (rectangle, square) that would not be logical
         */

        Circle c1 = (Circle)s1;                   // Downcast back to Circle
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getPerimeter());
        System.out.println(c1.getColor());
        System.out.println(c1.isFilled());
        System.out.println(c1.getRadius());

        /*
            Shape s2 = new Shape();
           Shape is already abstract, so it is not logical to
           assign an inaccurate value to a variable(which already abstract), it will give an error
        */


        Shape s3 = new Rectangle(1.0, 2.0, "red", false);   // Upcast
        System.out.println(s3);
        System.out.println(s3.getArea());
        System.out.println(s3.getPerimeter());
        System.out.println(s3.getColor());
        /*
        System.out.println(s3.getLength());
        it should be written like this  System.out.println(((Rectangle) s3).getLength());

         */

        Rectangle r1 = (Rectangle)s3;   // downcast
        System.out.println(r1);
        System.out.println(r1.getArea());
        System.out.println(r1.getColor());
        System.out.println(r1.getLength());

        Shape s4 = new Square(6.6);     // Upcast
        System.out.println(s4);
        System.out.println(s4.getArea());
        System.out.println(s4.getColor());
          /*
          System.out.println(s4.getSide());
          it same error like in 39 line
          System.out.println(((Square) s4).getSide());//correct version
         */

// Take note that we downcast Shape s4 to Rectangle,
//  which is a superclass of Square, instead of Square
        Rectangle r2 = (Rectangle)s4;
        System.out.println(r2);
        System.out.println(r2.getArea());
        System.out.println(r2.getColor());
       /*
        System.out.println(r2.getSide());
        getSide has square class, and rectangle not extends Square class
        */
        System.out.println(r2.getLength());

// Downcast Rectangle r2 to Square
        Square sq1 = (Square)r2;
        System.out.println(sq1);
        System.out.println(sq1.getArea());
        System.out.println(sq1.getColor());
        System.out.println(sq1.getSide());
        System.out.println(sq1.getLength());


        System.out.println("//////////////////////////////////////////************************");
        Movable m1 = new MovablePoint(4, 3, 7, 8);
        System.out.println(m1);
        m1.moveLeft();
        System.out.println(m1);

        Movable m2 = new MovableCircle(-1, 0, 3, 4, 10);
        System.out.println(m2);
        m2.moveRight();
        System.out.println(m2);
    }
}
//What is the usage of the abstract method and abstract class?
/*
abstract keyword is used to create a abstract class and method.
Abstract class in java can't be instantiated. An abstract class is mostly used to provide
a base for subclasses to extend and implement the abstract methods and override or use the
implemented methods in abstract class.
 */