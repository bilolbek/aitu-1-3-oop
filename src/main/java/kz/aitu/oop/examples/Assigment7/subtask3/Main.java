package kz.aitu.oop.examples.Assigment7.subtask3;

public class Main {
    public static void main(String[] args) {

        /////////////////////////////////////////////
        GeometricObject gO1 = new Circle2(5.0);
        System.out.println(gO1);
        System.out.println("Perimeter = " + gO1.getPerimeter());
        System.out.println("Area = " + gO1.getArea());

        Resizable gO2 = new ResizableCircle(5.0);
        System.out.println(gO2);
        gO2.resize(50);

    }
}
