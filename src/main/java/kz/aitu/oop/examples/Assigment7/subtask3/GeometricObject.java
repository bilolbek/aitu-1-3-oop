package kz.aitu.oop.examples.Assigment7.subtask3;

public interface GeometricObject {
    double getPerimeter();

    double getArea();
}
