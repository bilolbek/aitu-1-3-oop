package kz.aitu.oop.examples.Assigment7.subtask3;

public class ResizableCircle extends Circle2 implements Resizable {
    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public void resize(int percent) {
        radius *= percent / 100.0;
    }
}
