package kz.aitu.oop.examples.Assigment7;

public interface Movable {
    void moveUp();

    void moveDown();

    void moveLeft();

    void moveRight();

}
