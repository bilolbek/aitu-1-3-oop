package kz.aitu.oop.examples.jdbc;

import kz.aitu.oop.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String url = "jdbc:mysql://127.0.0.1:3306/aitu";
        String username = "root";
        String password = "123456";

        try {
            //Class.forName("com.mysql.jdbc.Driver");

            Connection connection = DriverManager.getConnection(url, username, password);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select * from student");


            List<Student> studentList = new ArrayList<>();
            while(resultSet.next()) {
                Student student = new Student();
                student.setId(resultSet.getInt("id"));
                student.setName(resultSet.getString("name"));
                student.setAge(resultSet.getInt("age"));
                student.setPoint(resultSet.getDouble("point"));

                System.out.println(student);
            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch (SQLException sql) {
            sql.printStackTrace();
        }

    }
}
