package kz.aitu.oop.examples;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AssignmentOne {
    public static void main(String[] args) throws Exception {

        AssignmentOne fileReader1 = new AssignmentOne();
        fileReader1.getFile().getPoints();
    }
    public Shape getFile() throws FileNotFoundException {

        File file = new File("/Users/sardo/Desktop/file1.txt" );
        Scanner sc = new Scanner(file);
        Shape shp = new Shape();
        Double x1 = null,y1 = null;
        while (sc.hasNext()) {
            if (sc.hasNextFloat()) {
                if (x1==null){
                    x1=sc.nextDouble();
                } else if (y1==null){
                    y1=sc.nextDouble();
                }
                if (x1!=null && y1!=null){
                    shp.addPoint(new Point(x1,y1));
                    x1 = null;
                    y1 = null;
                }
            }
        }
        return shp;
    }
}
