package kz.aitu.oop.examples.exception;

import kz.aitu.oop.entity.Student;

public class Main {


    public static void main(String[] args) {

        int a = 10;
        int b = 5;
        int c = 4;

        int result = 100;
        try {
            result = a / 0;
        } catch(ArithmeticException e) {
            System.err.println("WE HAVE ERROR! " + e.getMessage());
            result = 0;
        }

        System.out.println(result);


        System.out.println("HELLO WORLD!");


        System.out.println("******************************");

        Student student = null;

        try {
            System.out.println(getName(null));
        } catch (NullPointerException nlp) {
            System.out.println("error student is null");
        }


        //login("123123", "123123123");

        try {
            compute(5);
        } catch (LogicNotFoundException e) {
            System.out.println("ERROR " + e.toString());
        }
    }



    public static String getName(Student student) throws NullPointerException {
        //if(student == null) return null;
        /*try {
            return student.getName();
        } catch (NullPointerException nlp) {
            return "LALALALLA";
        }*/

        return student.getName();
    }


    public static boolean login(String username, String password) {
        String u = "root";
        String p = "12345678";

        if(username.equals(u) && password.equals(p)) return true;
        else {
            throw new ArithmeticException("just a joke");
        }



    }

    public static void compute(int a) throws LogicNotFoundException {
        if(a > 10) {
            System.out.println("OK");
        } else {
            throw new LogicNotFoundException(a);
        }
    }
}
