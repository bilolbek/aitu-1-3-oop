package kz.aitu.oop.examples.Assignment3;

class NewSpecialString {

    private String[] ar_val;

    public NewSpecialString(String[] values) {
        this.ar_val = values;
    }

    public int length() {
        return ar_val.length;
    }

    public String valueAt(int position) {
        int start = 0;
        int end = ar_val.length;
        if ((position > start) && (position < end)) {
            return ar_val[position];
        } else return "-1";
    }

    public boolean contains(String value) {
        int start = 0;
        int end = ar_val.length;
        int flag = 0;
        for (int i = start; i < end; i++) {
            if (value == ar_val[i]) {
                flag++;
            }
        }
        if (flag == 0) return false;
        else return true;
    }

    public int count(String value) {
        int start = 0;
        int end = ar_val.length;
        int flag = 0;
        for (int i = start; i < end; i++) {
            if (value == ar_val[i]) {
                flag++;
            }
        }
        return flag;
    }

    public void show() {
        int start = 0;
        int end = ar_val.length;
        for (int i = start; i < end; i++) {
            System.out.println(ar_val[i]);
        }
    }


}
