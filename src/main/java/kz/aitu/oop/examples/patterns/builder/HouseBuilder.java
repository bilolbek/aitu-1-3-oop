package kz.aitu.oop.examples.patterns.builder;

public class HouseBuilder {
    private House house;

    public HouseBuilder(String name) {
        house = new House(name);
    }

    public HouseBuilder addGarden() {
        house.setHasGarden(true);
        return this;
    }

    public HouseBuilder addGarage() {
        house.setHasGarage(true);
        return this;
    }

    public HouseBuilder addStatues() {
        house.setHasStatues(true);
        return this;
    }

    public HouseBuilder addSwimmingPool() {
        house.setHasSwimmingPool(true);
        return this;
    }

    public House make() {
        return house;
    }
}
