package kz.aitu.oop.examples.patterns.builder;

public class House {
    private String name;
    private boolean hasGarden;
    private boolean hasSwimmingPool;
    private boolean hasGarage;
    private boolean hasStatues;

    public House(String name) {
        this.name = name;
    }

    public House(String name, boolean hasGarden, boolean hasSwimmingPool, boolean hasGarage, boolean hasStatues) {
        this.name = name;
        this.hasGarden = hasGarden;
        this.hasSwimmingPool = hasSwimmingPool;
        this.hasGarage = hasGarage;
        this.hasStatues = hasStatues;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasGarden() {
        return hasGarden;
    }

    public void setHasGarden(boolean hasGarden) {
        this.hasGarden = hasGarden;
    }

    public boolean isHasSwimmingPool() {
        return hasSwimmingPool;
    }

    public void setHasSwimmingPool(boolean hasSwimmingPool) {
        this.hasSwimmingPool = hasSwimmingPool;
    }

    public boolean isHasGarage() {
        return hasGarage;
    }

    public void setHasGarage(boolean hasGarage) {
        this.hasGarage = hasGarage;
    }

    public boolean isHasStatues() {
        return hasStatues;
    }

    public void setHasStatues(boolean hasStatues) {
        this.hasStatues = hasStatues;
    }
}
