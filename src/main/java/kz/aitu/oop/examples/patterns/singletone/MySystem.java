package kz.aitu.oop.examples.patterns.singletone;

public class MySystem {
    private static String name;
    private int count;
    private boolean flag;

    private static MySystem mysystemInstance = null;

    private MySystem() {
    }

    public static MySystem getMySystem() {
        if(mysystemInstance == null) mysystemInstance = new MySystem();
        return mysystemInstance;
    }

    @Override
    public String toString() {
        return "MySystem{" +
                "name='" + name + '\'' +
                ", count=" + count +
                ", flag=" + flag +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        MySystem.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}

