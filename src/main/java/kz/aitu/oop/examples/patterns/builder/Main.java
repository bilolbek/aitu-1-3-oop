package kz.aitu.oop.examples.patterns.builder;

public class Main {public static void main(String[] args) {


    HouseBuilder houseBuilder = new HouseBuilder("HOUSE-1");


    House house2 = new House("simple", true, false, false, true);


    House house = houseBuilder
            .addGarage()
            .addSwimmingPool()
            .make();


}
}

