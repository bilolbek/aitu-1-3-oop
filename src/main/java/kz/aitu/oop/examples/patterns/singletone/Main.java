package kz.aitu.oop.examples.patterns.singletone;

public class Main {
    public static void main(String[] args) {
        MySystem mySystem1 = MySystem.getMySystem();
        mySystem1.setName("system1");
        mySystem1.setCount(5);




        MySystem mySystem2 = MySystem.getMySystem();
        mySystem2.setName("system2");
        mySystem2.setCount(10);




        System.out.println(mySystem1);
        System.out.println(mySystem2);
    }
}
