package kz.aitu.oop.examples.Assigment5;

import org.springframework.aop.scope.ScopedProxyUtils;

public class Main {
    public static void main(String[] args) {

        Shape sh1 = new Shape();
        sh1.setColor("White");
        sh1.setFilled(true);
        System.out.println(sh1.toString());


        Circle cr1 = new Circle();
        cr1.setColor("Green");
        cr1.setFilled(false);
        cr1.setRadius(5);
        System.out.println(cr1.toString());
        System.out.println(cr1.getPerimeter());
        System.out.println(cr1.getArea());


        Rectangle rc1 = new Rectangle();
        rc1.setColor("White");
        rc1.setFilled(true);
        rc1.setWidth(10);
        rc1.setLength(20);
        System.out.println(rc1.toString());
        System.out.println(rc1.getPerimeter());
        System.out.println(rc1.getArea());


        Square cq1 = new Square();
        cq1.setColor("White");
        cq1.setFilled(true);
        cq1.setSide(10);
        System.out.println(cq1.toString());
        System.out.println(cq1.getPerimeter());
        System.out.println(cq1.getArea());

    }
}
