package kz.aitu.oop.examples;

import java.util.LinkedList;

public class Shape {
    public static void main(String[] args) throws Exception {
        Shape s1 = new Shape();
        Point p1 = new Point(-3, 9);
        s1.addPoint(p1);
        Point p2 = new Point(-8, 7);
        s1.addPoint(p2);
        Point p3 = new Point(-12, 4);
        s1.addPoint(p3);
        Point p4 = new Point(4, -6);
        s1.addPoint(p4);
        s1.addPoint(new Point(5, 3));
        System.out.println(s1.getAverage());
        s1.getPoints();
        s1.getLongest();
    }

    private LinkedList<Point> shape = new LinkedList<Point>();

    public Shape() {
    }

    public void addPoint(Point point) {
        shape.add(point);
    }

    public void getPoints() {
        for (int i = 0; i < shape.size(); i++) {
            System.out.println(shape.get(i).getX() + " " + shape.get(i).getY());
        }
    }

    public double calculatePerimeter() {
        double perimeter = 0;
        for (int i = 0; i < shape.size() - 1; i++) {
            perimeter += shape.get(i).distance(shape.get(i + 1));
        }
        return perimeter;
    }

    public double getLongest() {

        double max = shape.get(0).distance(shape.get(1));
        for (int i = 0; i < shape.size(); i++) {
            if (shape.get(i).distance(shape.get(i + 1)) > max)
                max = shape.get(i).distance(shape.get(i + 1));
        }
        if (shape.get(shape.size()).distance(shape.get(0)) > max)
            max = shape.get(shape.size()).distance(shape.get(0));

        return max;
    }

    public double getAverage() {
        return this.calculatePerimeter() / shape.size();
    }


}
